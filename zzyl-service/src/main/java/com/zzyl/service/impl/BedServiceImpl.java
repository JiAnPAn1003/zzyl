package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.zzyl.dto.BedDto;
import com.zzyl.entity.Bed;
import com.zzyl.enums.BasicEnum;
import com.zzyl.exception.BaseException;
import com.zzyl.mapper.BedMapper;
import com.zzyl.service.BedService;
import com.zzyl.utils.ObjectUtil;
import com.zzyl.vo.BedVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BedServiceImpl implements BedService {

    @Autowired
    private BedMapper bedMapper;

    @Override
    public List<BedVo> getBedsByRoomId(Long roomId) {
        return bedMapper.getBedsByRoomId(roomId);
    }

    @Override
    public void addBed(BedDto bedDto) {
        Bed bed = BeanUtil.toBean(bedDto, Bed.class);
        bed.setCreateTime(LocalDateTime.now());
        bed.setCreateBy(1L);
        bed.setBedStatus(0);
        try {
            bedMapper.addBed(bed);
        } catch (Exception e) {
            throw new BaseException(BasicEnum.BED_INSERT_FAIL);
        }
    }

    @Override
    public BedVo getBedById(Long id) {
        return BeanUtil.toBean(bedMapper.getBedById(id), BedVo.class);
    }

    @Override
    public void updateBed(BedDto bedDto) {
        BedVo bedVo = getBedById(bedDto.getId());
        bedVo.setSort(bedDto.getSort());
        bedVo.setBedNumber(bedDto.getBedNumber());
        if (ObjectUtil.isNotEmpty(bedDto.getBedStatus())) {
            bedVo.setBedStatus(bedDto.getBedStatus());
        }
        Bed bed = BeanUtil.toBean(bedVo, Bed.class);
        bed.setCreateTime(LocalDateTime.now());
        bed.setCreateBy(1L);
        bedMapper.updateBed(bed);
    }

    @Override
    public void deleteBedById(Long id) {
        bedMapper.deleteBedById(id);
    }
}

