package com.zzyl.mapper;

import com.zzyl.entity.Bed;
import com.zzyl.vo.BedVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BedMapper {

    List<BedVo> getBedsByRoomId(Long roomId);

    /**
     * 增加床位
     *
     * @param bed 床位对象
     */
    void addBed(Bed bed);

    /**
     * 根据ID查询床位
     *
     * @param id
     * @return
     */
    Bed getBedById(Long id);

    void updateBed(Bed bed);

    void deleteBedById(Long id);
}

