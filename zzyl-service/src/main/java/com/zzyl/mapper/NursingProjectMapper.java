package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.entity.NursingProject;
import com.zzyl.vo.NursingProjectVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 护理项目Mapper接口
 */
@Mapper
public interface NursingProjectMapper {

    Page<NursingProjectVo> selectByPage(String name, Integer status);

}